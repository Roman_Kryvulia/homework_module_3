
USE R_K_module_3
GO
CREATE TABLE [my_shema].[football_club]
(
id_football_club INT IDENTITY PRIMARY KEY,
full_name nvarchar(50) NOT NULL,
short_name varchar(3) NOT NULL,
foundation date NOT NULL,
city nvarchar(50) NOT NULL,
name_stadium nvarchar(max) NOT NULL,
website varchar(50) NOT NULL,
telefon varchar(50) NOT NULL,
fax varchar(50) NOT NULL,
last_win date NULL,
history nvarchar(max) NOT NULL DEFAULT('no_information'),
instered_date date NOT NULL DEFAULT getdate(),
update_date date NULL,
CONSTRAINT UQ_football_club_website UNIQUE (website),
CONSTRAINT UQ_football_club_telefon UNIQUE (telefon),
CONSTRAINT UQ_football_club_fax UNIQUE (fax)
)
GO

CREATE TABLE [my_shema].[player]
(
id_player INT IDENTITY,
firstname nvarchar(50) NOT NULL,
surname nvarchar(50) NOT NULL,
date_of_birth date NOT NULL,
place_of_birth nvarchar(50) NOT NULL,
nationality nvarchar(50) NOT NULL,
position nvarchar(50) NOT NULL,
player_number int NULL,
height decimal(3,2) NOT NULL DEFAULT (0),
achievements nvarchar(max) NULL,
email varchar(50) NOT NULL,
player_agents nvarchar(50) NULL,
contract_until date NULL,
market_value float NULL,
id_football_club int NULL,
instered_date date NOT NULL DEFAULT getdate(),
update_date date NULL,
CONSTRAINT PK_player_id_player PRIMARY KEY (id_player),
CONSTRAINT UQ_player__email UNIQUE (email),
)
GO
