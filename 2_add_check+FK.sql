USE [R_K_module_3]
GO

ALTER TABLE [my_shema].[player]
 ADD CONSTRAINT CK_player_player_number CHECK (player_number>0)
GO
  
ALTER TABLE [my_shema].[player]
 ADD CONSTRAINT CK_player_hight CHECK (height>=0)
 GO

ALTER TABLE [my_shema].[player]
WITH CHECK ADD CONSTRAINT FK_player_football_club FOREIGN KEY (id_football_club)
REFERENCES [my_shema].[football_club] (id_football_club)
 ON UPDATE CASCADE
 ON DELETE SET NULL
 GO

