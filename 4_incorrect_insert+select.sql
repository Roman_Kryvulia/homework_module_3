 USE [R_K_module_3]
GO
 --unsuccessful insert (telefon UQ) 
INSERT INTO [my_shema].[football_club] (full_name, short_name, foundation, city, name_stadium, website, telefon, fax) VALUES 
 ('everton FC','EVE', '18780601', 'liverpool', 'Goodison Park', 'evertonfc.com','+44(151)2632361', '+44(151)2869')
  GO
   
   
    SELECT * FROM [my_shema].[football_club]
 GO
   --successful insert (changed telefon)

INSERT INTO [my_shema].[football_club] (full_name, short_name, foundation, city, name_stadium, website, telefon, fax) VALUES 
 ('everton FC','EVE', '18780601', 'liverpool', 'Goodison Park', 'evertonfc.com','+44(151)3302', '+44(151)2869')
  GO
   SELECT * FROM [my_shema].[football_club]
 GO


 --unsuccessful insert (player_hight CHECK (height>=0))
  SELECT * FROM [my_shema].[player]
 GO
 INSERT INTO [my_shema].[player] VALUES 
 ('marek','hamsik', '19870722', 'Banska Bystryca', 'Slovakian',
 'central midfield', 17, -0.5, 'italian cup winner', 'mhamsik@gmail.com','CSM Agency', '20200630', 35000000, 2, '20170504', '20170717')
  GO
 
  --successful insert (changed height)

 INSERT INTO [my_shema].[player] VALUES 
 ('marek','hamsik', '19870722', 'Banska Bystryca', 'Slovakian',
 'central midfield', 17, 1.83, 'italian cup winner', 'mhamsik@gmail.com','CSM Agency', '20200630', 35000000, 2, '20170504', '20170717')
  GO
     SELECT * FROM [my_shema].[player]
 GO



 --unsuccessful insert (add non-existent id_football_club)
    SELECT * FROM [my_shema].[football_club]
 GO

 INSERT INTO [my_shema].[player] (firstname, surname, date_of_birth, place_of_birth, nationality, position, email, id_football_club)  VALUES 
 ('mykola','shaparenko', '19981004', 'Velyka Novosilka', 'Ukrainian', 'central midfield', 'mykolka1998@ua.fm', 8)
  GO
    
	--successful insert (changed id_football_club)
INSERT INTO [my_shema].[player] (firstname, surname, date_of_birth, place_of_birth, nationality, position, email, id_football_club)  VALUES 
 ('mykola','shaparenko', '19981004', 'Velyka Novosilka', 'Ukrainian', 'central midfield', 'mykolka1998@ua.fm', 3)
  GO
  
 SELECT * FROM [my_shema].[player]
 GO
 SELECT * FROM [my_shema].[football_club]
 GO

 --successful select (for join) 
 SELECT fc.full_name,fc.city, p.surname, p.date_of_birth, p.position
 FROM [my_shema].[player] p JOIN [my_shema].[football_club] fc
 ON p.id_football_club = fc.id_football_club
 WHERE p.position = 'central midfield' AND p.date_of_birth > '1987-01-01'
GO

  