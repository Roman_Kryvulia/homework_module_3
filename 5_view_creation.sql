USE [R_K_module_3]
GO

--- create view
 CREATE OR ALTER VIEW [my_shema].[stadium]
 (
  [name_stadium],
  [city],
  [telefon]
  )
 AS
 SELECT
 [name_stadium],
  [city],
  [telefon]
 FROM [my_shema].[football_club]
  GO

 CREATE OR ALTER VIEW [my_shema].[email_player]
 (
  [firstname],
  [surname],
  [email]
  )
 AS
 SELECT
[firstname],
  [surname],
  [email]
 FROM [my_shema].[player]
 GO

 --using view
  SELECT * FROM [my_shema].[stadium]
go
 SELECT * FROM [my_shema].[email_player]
go