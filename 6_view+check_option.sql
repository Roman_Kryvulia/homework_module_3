USE [R_K_module_3]
GO

--create new view with check option
 CREATE OR ALTER VIEW [my_shema].[no-moscow_stadium]
 (
 [full_name],
 [short_name],
 [foundation],
 [city],
 [name_stadium],
 [website],
 [telefon],
 [fax]
  )
 AS
 SELECT
 [full_name],
 [short_name],
 [foundation],
 [city],
 [name_stadium],
 [website],
 [telefon],
 [fax]
 FROM [my_shema].[football_club]
 WHERE [city] != 'moscow'
 WITH CHECK OPTION
 GO
   
   --using view
  SELECT * FROM [my_shema].[no-moscow_stadium]
GO
  
  --insert record (unsuccessful)
   INSERT INTO [my_shema].[no-moscow_stadium] (full_name, short_name, foundation, city, name_stadium, website, telefon, fax) VALUES 
 ('spartak moscow','SPA', '19220418', 'moscow', 'Otkrytie Arena', 'www.spartak.com','+7 (495) 530-95-09', '+7 (495) 530-95-08')
  GO

  --insert record (successful)
 INSERT INTO [my_shema].[no-moscow_stadium] (full_name, short_name, foundation, city, name_stadium, website, telefon, fax) VALUES 
 ('manchester united FC','MUN', '18780305', 'manchester', 'old trafort', 'www.maunutd.com','(0)1619 868 8000', '(0)1619 868 8000')
  GO
 
 
 SELECT * FROM [my_shema].[no-moscow_stadium]
go


 SELECT * FROM [my_shema].[football_club]
go

